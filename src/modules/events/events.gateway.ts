import {
  OnGatewayConnection,
  OnGatewayDisconnect,
  SubscribeMessage,
  WebSocketGateway,
  WebSocketServer,
  WsResponse,
} from '@nestjs/websockets';
import { from, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { User } from '../users/interfaces/user.interface';
import { JwtService } from '../auth/jwt/jwt.service';

@WebSocketGateway()
export class EventsGateway implements OnGatewayConnection, OnGatewayDisconnect {
  @WebSocketServer()
  server;

  connectedUsers: {} = {};

  constructor(
    private readonly jwtService: JwtService,
  ) {
  }

  async handleConnection(socket) {

    const user: User = await this.jwtService.verify(
      socket.handshake.query.token,
      true,
    );

    if (!this.connectedUsers.hasOwnProperty(user._id.toString())) {
      this.connectedUsers[user._id.toString()] = {};
    }

    this.connectedUsers[user._id.toString()][socket.client.id] = {
      rooms: [],
      client: socket,
    };
    socket.join(`user_${user._id.toString()}`);

    // Send list of connected users
    // this.server.emit('users', this.connectedUsers);
  }

  async handleDisconnect(socket) {
    const user: User = await this.jwtService.verify(
      socket.handshake.query.token,
      true,
    );

    if (!this.connectedUsers.hasOwnProperty(user._id.toString())) {
      this.connectedUsers[user._id.toString()] = {};
    }
    delete this.connectedUsers[user._id.toString()][socket.client.id];
    socket.leave(`user_${user._id.toString()}`);
    // Sends the new list of connected users
    // this.server.emit('users', this.connectedUsers);
  }

  @SubscribeMessage('message')
  async onMessage(client, data: any) {
    const event: string = 'message';

    client.broadcast
      .to(data.room)
      .emit(
        event,
        `${new Date().toISOString()}: ${client.id}: ${data.message}`,
      );

    return Observable.create(observer =>
      observer.next({
        event,
        data: `${new Date().toISOString()}: ${client.id}: ${data.message}`,
      }),
    );
  }

  @SubscribeMessage('join')
  async onRoomJoin(client, data: any): Promise<any> {
    client.join(data);
  }

  @SubscribeMessage('leave')
  onRoomLeave(client, data: any): void {
    client.leave(data);
  }

  @SubscribeMessage('events')
  findAll(client, data): Observable<WsResponse<number>> {
    return from([1, 2, 3]).pipe(map(item => ({ event: 'events', data: item })));
  }

  @SubscribeMessage('identity')
  async identity(client, data: number): Promise<number> {
    return data;
  }
}
