angular.module('app').component('usersComponent', {
  templateUrl: '/src/components/users/users.component.html',
  controller: function ($state, usersService) {
    this.editMode = ['root.users.new', 'root.users.edit'].indexOf($state.$current.name) !== -1;

    this.add = () => {
      $state.transitionTo('root.users.new');
      this.editMode = true;
    };

    this.update = (user_id) => {
      $state.transitionTo('root.users.edit', { user_id });
      this.editMode = true;
    };

    this.remove = (user_id) => {
      if(confirm('Вы действительно хотите удалить?')) {
        usersService.remove(user_id);
      }
    };
  },
  bindings: {
    data: '<'
  }
});