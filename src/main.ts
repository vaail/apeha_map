import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';

// import * as bodyParser from 'body-parser';
// import * as compression from 'compression';
// import * as rateLimit from 'express-rate-limit';
import * as express from 'express';
// import * as helmet from 'helmet';
import { UserSchema } from './modules/users/schemas/user.schema';
import * as mongoose from 'mongoose';
import { User } from './modules/users/interfaces/user.interface';
import { APP_CONFIG } from './config';
import { RolesGuard } from './modules/common/guards/roles.guard';

import * as Adapter from 'socket.io-adapter';
const oldBroadcast = Adapter.prototype.broadcast;
// @ts-ignore
Adapter.prototype.broadcast = function(packet, opts) {
  opts.except = [];
  oldBroadcast.bind(this)(packet, opts);
};

// const app = express();
// app.use(express.static('src/client'));
// app.use(helmet())
//   .use(compression())
//   .use(bodyParser.json())
//   .use(bodyParser.urlencoded({
//       extended: true,
//   }))
//   .use(rateLimit({
//     windowMs: 15 * 60 * 1000, // 15 minutes
//     max: 100000, // limit each IP to 100 requests per windowMs
//   }));

const members = [
  {
    username: 'cikori',
    password: 'n9hsu4ve',
    role: 'member',
  },
  {
    username: '***FILOSOF***',
    password: 'l73p9sgo',
    role: 'member',
  },
  {
    username: 'Достоевский',
    password: '661ah0ff',
    role: 'member',
  },
  {
    username: 'bloodangel111',
    password: '5alixlyw',
    role: 'member',
  },
  {
    username: 'СТРАННИК_В_НОЧИ',
    password: '7fhq2yyb',
    role: 'member',
  },
  {
    username: 'мойша',
    password: 'ly82tm7a',
    role: 'member',
  },
  {
    username: 'Lisses',
    password: 'r9yy3e6v',
    role: 'member',
  },
  {
    username: 'GIVIK',
    password: '10us49un',
    role: 'member',
  },
  {
    username: 'RbIZHuK',
    password: 'y12zhu3p',
    role: 'member',
  },
  {
    username: '*Vendetta*',
    password: 'cok4sf67',
    role: 'member',
  },
  {
    username: 'DJ SVEN',
    password: 'o6eaj4u5',
    role: 'member',
  },
  {
    username: 'ganja legaliz',
    password: 'klkbbg1i',
    role: 'member',
  },
  {
    username: 'MEGADIGGER 2',
    password: 'jchwq6fm',
    role: 'member',
  },
  {
    username: 'climacool',
    password: 'rjpdi3eh',
    role: 'member',
  },
  {
    username: 'STIKSS',
    password: 'oaya8zue',
    role: 'member',
  },
  {
    username: '!!Spitamen!!',
    password: 'qylqh4kn',
    role: 'member',
  },
  {
    username: 'volodimir',
    password: '8737rfl7',
    role: 'member',
  },
  {
    username: 'GeorG 87',
    password: 'ryjzbqxe',
    role: 'member',
  },
  {
    username: 'kim.pz',
    password: '4ng4ywv4',
    role: 'member',
  },
  {
    username: '!GRAND!',
    password: 'zi3091wf',
    role: 'member',
  },
  {
    username: 'И.Н.А.',
    password: 't7mdrd6z',
    role: 'member',
  },
  {
    username: 'fofan040567',
    password: '6ju9rfdn',
    role: 'member',
  },
  {
    username: 'забавная писюлька',
    password: 'm0oausy8',
    role: 'member',
  },
  {
    username: 'SONIK.',
    password: 'b4zqhwao',
    role: 'member',
  },
  {
    username: 'fanatik.',
    password: 'd82hh08v',
    role: 'member',
  },
  {
    username: 'NoobS',
    password: 'yjazveal',
    role: 'member',
  },
  {
    username: 'Hoshigake Kisame',
    password: 'kvw1x7mv',
    role: 'member',
  },
  {
    username: 'ДЕРЗЮЛЬКА ИЗ МОСКВЫ',
    password: 'mr3cql1r',
    role: 'member',
  },
  {
    username: 'krasilnikov',
    password: 'kl24j41c',
    role: 'member',
  },
  {
    username: '--DayWalkeR--',
    password: 'kjocdl5j',
    role: 'member',
  },
  {
    username: 'korstap',
    password: 'qm8knxqq',
    role: 'member',
  },
  {
    username: 'Lineman on the Duty.',
    password: 'glk9m1y5',
    role: 'member',
  },
  {
    username: 'NALOGOPLATELSCHIK',
    password: 'trryqi5h',
    role: 'member',
  },
  {
    username: '!_хель_!',
    password: '3vgnsk8x',
    role: 'member',
  },
  {
    username: 'MYXA89',
    password: 'h0pxahx8',
    role: 'member',
  },
  {
    username: 'Одиссей',
    password: 'ih9u8a9e',
    role: 'member',
  },
  {
    username: 'Johnny Walker',
    password: 'w3rj26bo',
    role: 'member',
  },
  {
    username: '**Хулиганка**',
    password: 'n1nrqpyn',
    role: 'member',
  },
  {
    username: 'Katrin',
    password: 'k8f3ggpl',
    role: 'member',
  },
  {
    username: 'Лялька',
    password: 'g7leuxzg',
    role: 'member',
  },
  {
    username: 'Стильный',
    password: '6p7v687w',
    role: 'member',
  },
  {
    username: 'Esterno',
    password: '9iq6wd3b',
    role: 'member',
  },
  {
    username: 'Балаган',
    password: 'ye8w76fu',
    role: 'member',
  },
  {
    username: '~ПИВКО~',
    password: 'e9wj25oi',
    role: 'member',
  },
  {
    username: '-ANDREIKA-',
    password: 'x0cnt9en',
    role: 'member',
  },
  {
    username: '_ARCHE_',
    password: 'eun5aihl',
    role: 'member',
  },
  {
    username: 'метательная_блоха',
    password: '0hb9xm37',
    role: 'member',
  },
  {
    username: 'Слуцкий2016',
    password: 'hbr8i4df',
    role: 'member',
  },
  {
    username: '-Shaker-',
    password: '3bwgz71v',
    role: 'member',
  },
  {
    username: 'Григорич',
    password: '8cweta9v',
    role: 'member',
  },
  {
    username: 'Сентябринка',
    password: 'srkxog5f',
    role: 'member',
  },
  {
    username: 'Дед Капай',
    password: 'qqbjam36',
    role: 'member',
  },
  {
    username: '~Delirium~',
    password: 'omwe4ewr',
    role: 'member',
  },
  {
    username: 'Борбос3',
    password: 'dsdlmk62',
    role: 'member',
  },
  {
    username: 'Александр батькович',
    password: '2q0d85ir',
    role: 'member',
  },
  {
    username: 'Арнор !',
    password: 'qkpf0e4b',
    role: 'member',
  },
  {
    username: 'Malkovian',
    password: '4n06gkse',
    role: 'member',
  },
  {
    username: '*Scarlet*',
    password: 'kgav4rwq',
    role: 'member',
  },
  {
    username: 'Парамоныч',
    password: 'hhxc1ioj',
    role: 'member',
  }
];

async function bootstrap() {
  const nestApp = await NestFactory.create(AppModule);
  if (true) {
    await mongoose.connect(APP_CONFIG.databaseURL);
    const UserModel = mongoose.model('User', UserSchema);
    const admin = await UserModel.findOne({ username: '!_хель_!' });
    // (JSON.stringify(nics.split('\n').map(v => v.trim()).map(v => v.split(' ').slice(1, -1).join(' '))
    // .reduce((rest, nik) => {
    // rest.push({ username: nik, password: Math.random().toString(36).slice(-8), role: 'member'
    // }); return rest; }, []), null, 2))
    // .replace(new RegExp(/\"(.*)\":/, 'mg'), "$1:").replace(new RegExp(/"/, 'mg'), "'").replace(new RegExp(/'member'/, 'mg'), "'member',")
    if (!admin) {
      await UserModel.create({
        username: '!_хель_!',
        password: 'mqvo9xhj',
        role: 'admin',
      } as User);
    }

    const q = members.map(member =>
      UserModel
        .findOne({ username: member.username })
        // @ts-ignore
        .then(user => {
          if (user) {
            return Promise.resolve();
          } else {
            return UserModel.create(member);
          }
        }));

    await Promise.all(q);
  }
  //
  // Add a route prefix
  nestApp.setGlobalPrefix('api');
  nestApp.enableCors();

  const rolesGuard = nestApp.select(AppModule).get(RolesGuard);
  nestApp.useGlobalGuards(rolesGuard);
  nestApp.use(express.static('src/client'));

  await nestApp.listen(3666);
}
bootstrap();
