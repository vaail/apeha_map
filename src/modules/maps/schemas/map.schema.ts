import { Schema } from 'mongoose';

const map = new Schema({
  name: { type: String, required: true },
  color: { type: String, default: 'blue' },
  userAccess: { type: [String], default: [] },
  map: { type: [Object], default: [] },
  created_at: { type: Date, default: Date.now },
  updated_at: { type: Date, default: Date.now },
});

/**
 * On every save, add the date
 */
map.pre('save', function(next) {
  const currentDate = new Date();

  // @ts-ignore
  this.updated_at = currentDate;
  next();
});

/**
 * Serialize map to send
 */
map.methods.serialize = (_map) => {
  return {
    _id: _map._id,
    name: _map.name,
    color: _map.color,
    map: _map.map,
    userAccess: _map.userAccess,
  };
};

export const MapSchema = map;
