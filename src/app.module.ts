import { MiddlewareConsumer, Module, NestModule } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { EventsModule } from './modules/events/events.module';

import { AuthModule } from './modules/auth/auth.module';
import { UsersModule } from './modules/users/users.module';

import { APP_CONFIG } from './config';
import { AuthMiddleware } from './modules/common/middlewares/auth.middleware';
import { UsersController } from './modules/users/users.controller';
import { MapsModule } from './modules/maps/maps.module';
import { MapsController } from './modules/maps/maps.controller';
import { RolesGuard } from './modules/common/guards/roles.guard';

@Module({
  imports: [
    MongooseModule.forRoot(APP_CONFIG.databaseURL),
    AuthModule,
    UsersModule,
    MapsModule,
    EventsModule,
  ],
  controllers: [AppController],
  providers: [
    AppService,
    RolesGuard,
  ],
})
export class AppModule implements NestModule {
  public configure(consumer: MiddlewareConsumer) {
    consumer.apply(AuthMiddleware).forRoutes(UsersController);
    consumer.apply(AuthMiddleware).forRoutes(MapsController);
  }
}
