angular.module('app').component('userFormComponent', {
  templateUrl: '/src/components/user-form/user-form.component.html',
  controller: function (usersService) {
    this.form = {};
    this.roles = [
      { id: 'member', label: 'Участник' },
      { id: 'admin', label: 'Админ' },
    ];
    this.save = () => {
      this.item._id
        ? usersService.update(this.item._id, {
          _id: this.item._id,
          ...this.form
        })
        : usersService.create({
          ...this.form
        });
    };

    this.$onChanges = (data) => {
      if (data.item.currentValue) {
        this.form = {...data.item.currentValue}
      }
    };
  },
  bindings: {
    item: '<'
  }
});