angular.module('app').component('loginComponent', {
  templateUrl: '/src/components/login/login.component.html',
  controller: function ($state, loginService, authManager) {
    this.form = {
      username: null,
      password: null,
    };
    this.login = () => {
      loginService.login(this.form.username, this.form.password)
        .then(() => {
          $state.transitionTo('root');
        })
        .catch((e) => {
          alert(e.data ? e.data.message : 'Error');
        });
    };

    if (authManager.isAuthenticated()) {
      $state.transitionTo('root');
    }
  },
  bindings: {}
});