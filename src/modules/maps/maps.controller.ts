import {
  Controller,
  Get,
  Post,
  Delete,
  Body,
  Request,
  HttpException,
  HttpStatus,
  Put, Req,
} from '@nestjs/common';

import { MapsService } from './maps.service';
import { Map } from './interfaces/map.interface';
import { Roles } from '../common/decorators/roles.decorator';
import { EventsGateway } from '../events/events.gateway';
import { ObjectId } from 'bson';

@Controller('maps')
@Roles('admin')
export class MapsController {
  constructor(
    private readonly mapsService: MapsService,
    private readonly eventsGateway: EventsGateway,
  ) {
  }

  @Get()
  @Roles('admin', 'member')
  async index(@Req() req): Promise<Map[]> {
    const findCriteria = {} as any;
    if (req.user.role !== 'admin') {
      findCriteria.userAccess = req.user._id;
    }

    const userId = req.user._id.toString();
    if (this.eventsGateway.connectedUsers.hasOwnProperty(userId)) {
      Object.keys(this.eventsGateway.connectedUsers[userId]).forEach(id => {
        const client = this.eventsGateway.connectedUsers[userId][id];
        if (client.rooms.indexOf('maps') === -1) {
          client.client.join('maps');
          client.rooms.push('maps');
        }
      });
    }

    return await this.mapsService.findAll(findCriteria);
  }

  @Get(':id')
  @Roles('admin', 'member')
  async show(@Request() req): Promise<Map> {
    const id = req.params.id;
    if (!id)
      throw new HttpException(
        'ID parameter is missing',
        HttpStatus.BAD_REQUEST,
      );

    const findCriteria = {
      _id: new ObjectId(id),
    } as any;
    if (req.user.role !== 'admin') {
      findCriteria.userAccess = req.user._id;
    }

    let map;
    try {
      map = await this.mapsService.findOne(findCriteria);
    } catch (e) {
      map = null;
    }
    if (!map)
      throw new HttpException(
        `The map with the id: ${id} does not exists`,
        HttpStatus.BAD_REQUEST,
      );

    return map;
  }

  @Post()
  async create(@Body() body, @Request() req) {
    if (!body || (body && Object.keys(body).length === 0))
      throw new HttpException('Missing informations', HttpStatus.BAD_REQUEST);

    const item = await this.mapsService.create(body);

    const userId = req.user._id.toString();
    if (this.eventsGateway.connectedUsers.hasOwnProperty(userId)) {
      const ids = Object.keys(this.eventsGateway.connectedUsers[userId]);
      if (ids.length) {
        this.eventsGateway.connectedUsers[userId][ids[0]].client.broadcast
          .to('maps')
          .emit(
            'maps',
            ['create', item._id.toString()],
          );
      }
    }
  }

  @Put(':id')
  async update(@Request() req) {
    const id = req.params.id;
    if (!id)
      throw new HttpException(
        'ID parameter is missing',
        HttpStatus.BAD_REQUEST,
      );

    await this.mapsService.update(id, req.body);

    const userId = req.user._id.toString();
    if (this.eventsGateway.connectedUsers.hasOwnProperty(userId)) {
      const ids = Object.keys(this.eventsGateway.connectedUsers[userId]);
      if (ids.length) {
        this.eventsGateway.connectedUsers[userId][ids[0]].client.broadcast
          .to('maps')
          .emit(
            'maps',
            ['update', req.params.id],
          );
      }
    }
  }

  @Put(':id/action')
  @Roles('admin', 'member')
  async action(@Request() req) {
    const id = req.params.id;
    if (!id)
      throw new HttpException(
        'ID parameter is missing',
        HttpStatus.BAD_REQUEST,
      );

    await this.mapsService.action(id, { ...req.body, user: req.user });

    const userId = req.user._id.toString();
    if (this.eventsGateway.connectedUsers.hasOwnProperty(userId)) {
      const ids = Object.keys(this.eventsGateway.connectedUsers[userId]);
      if (ids.length) {
        this.eventsGateway.connectedUsers[userId][ids[0]].client.broadcast
          .to('maps')
          .emit(
            'maps',
            ['update', req.params.id],
          );
      }
    }
  }

  @Delete(':id')
  public async delete(@Request() req) {
    const id = req.params.id;
    if (!id)
      throw new HttpException(
        'ID parameter is missing',
        HttpStatus.BAD_REQUEST,
      );

    await this.mapsService.delete(id);

    const userId = req.user._id.toString();
    if (this.eventsGateway.connectedUsers.hasOwnProperty(userId)) {
      const ids = Object.keys(this.eventsGateway.connectedUsers[userId]);
      if (ids.length) {
        this.eventsGateway.connectedUsers[userId][ids[0]].client.broadcast
          .to('maps')
          .emit(
            'maps',
            ['delete', req.params.id],
          );
      }
    }
  }
}
