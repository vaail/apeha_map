const app = angular.module('app', ['ui.router', 'uiRouterStyles', 'angular-jwt', 'angular-storage']);

app.factory('authInterceptor', ['$q', 'store', function ($q, store) {
  return {
    responseError: function (response) {
      if (response.status === 400 && response.data && response.data.message === 'Unauthorized access') {
        store.remove('authToken');
        window.location.pathname = "/";
      }
    }
  };
}]);
app.config(function(jwtOptionsProvider, $httpProvider) {
  jwtOptionsProvider.config({
    tokenGetter: ['options', 'store', (options, store) => {
      if (options && options.url.substr(options.url.length - 5) === '.html') {
        return null;
      }

      return store.get('authToken');
    }],
    loginPath: 'login',
    unauthenticatedRedirectPath: '/login',
    whiteListedDomains: ['localhost'],
  });
  $httpProvider.interceptors.push('authInterceptor');
  $httpProvider.interceptors.push('jwtInterceptor');
});

app.config(function($urlRouterProvider, $stateProvider) {
  $stateProvider.state({
    name: 'login',
    url: '/login',
    component: 'loginComponent',
    data: {
      css: '/src/components/login/styles.css',
    },
  });
  $stateProvider.state({
    name: 'root',
    url: '/',
    component: 'rootComponent',
    resolve: {
      _socket: function (socket, store) {
        return socket.connect(store.get('authToken'));
      },
      me: function(_socket, usersService) {
        return usersService.me();
      },
      maps: function(_socket, mapsService) {
        return mapsService.list();
      },
    },
    data: {
      requiresLogin: true,
      css: '/src/components/root/styles.css',
    },
  });
  $stateProvider.state({
    name: 'root.maps',
    url: 'maps',
    abstract: true,
  });
  $stateProvider.state({
    name: 'root.maps.new',
    url: '/new',
    component: 'mapFormComponent',
    resolve: {
      item: function() {
        return Promise.resolve({
          name: '',
          userAccess: []
        });
      },
      users: function(usersService) {
        return usersService.list();
      },
    },
  });
  $stateProvider.state({
    name: 'root.maps.view',
    url: '/:map_id',
    component: 'mapViewComponent',
    resolve: {
      item: function($stateParams, mapsService) {
        return mapsService.get($stateParams.map_id);
      },
    },
  });
  $stateProvider.state({
    name: 'root.maps.edit',
    url: '/:map_id/edit',
    component: 'mapFormComponent',
    resolve: {
      item: function($stateParams, mapsService) {
        return mapsService.get($stateParams.map_id);
      },
      users: function(usersService) {
        return usersService.list();
      },
    },
  });
  $stateProvider.state({
    name: 'root.users',
    url: 'users',
    component: 'usersComponent',
    resolve: {
      data: function(usersService) {
        return usersService.list();
      },
    },
    data: {
      css: '/src/components/users/styles.css',
    },
  });
  $stateProvider.state({
    name: 'root.users.new',
    url: '/new',
    component: 'userFormComponent',
    resolve: {
      item: function() {
        return Promise.resolve({
          username: '',
          role: 'member',
          password: '',
        });
      },
    },
  });
  $stateProvider.state({
    name: 'root.users.edit',
    url: '/:user_id',
    component: 'userFormComponent',
    resolve: {
      item: function($stateParams, usersService) {
        return usersService.read($stateParams.user_id);
      },
    },
  });
  $urlRouterProvider.otherwise('/');
});

app.run(function(authManager, $transitions) {
  authManager.checkAuthOnRefresh();
  authManager.redirectWhenUnauthenticated();

  $transitions.onSuccess({}, () => {
    setInterval(feather.replace, 10);
  });
});