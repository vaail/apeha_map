import { forwardRef, Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { MapSchema } from './schemas/map.schema';
import { MapsService } from './maps.service';
import { MapsController } from './maps.controller';
import { EventsModule } from '../events/events.module';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: 'Map', schema: MapSchema }]),
    forwardRef(() => EventsModule),
  ],
  controllers: [MapsController],
  providers: [MapsService],
  exports: [MapsService],
})
export class MapsModule {}
