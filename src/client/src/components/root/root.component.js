angular.module('app').component('rootComponent', {
  templateUrl: '/src/components/root/root.component.html',
  controller: function (loginService, $rootScope, socket) {
    this.$onChanges = (data) => {
      if (data.me.currentValue) {
        $rootScope.me = data.me.currentValue;
      }
    };
    this.logout = () => {
      loginService.logout();
    };
    $rootScope.$on('unauthenticated', () => {
      socket.close();
    });
  },
  bindings: {
    maps: '<',
    me: '<'
  }
});