angular.module('app').service('loginService', ['$http', 'store', '$rootScope', function($http, store, $rootScope) {
  const login = (username, password) => {
    return $http.post('http://157.230.101.189/api/auth/login', {
      username,
      password,
    })
      .then((res) => {
        store.set('authToken', res.data.tokens.accessToken);

        return res.data.user;
      });
  };

  const logout = () => {
    store.remove('authToken');
    $rootScope.$broadcast('unauthenticated');
  };

  return {
    login,
    logout
  };
}]);
