angular.module('app').service('usersService', ['$http', 'socket', function($http, socket) {
  let data = {};

  const baseUrl = `http://157.230.101.189/api/users`;
  let inited = false;

  const list = () => {
    if (!inited) {
      socket.on('users', (message) => {
        if (message[0] === 'update' || message[0] === 'create') {
          read(message[1]);
        } else if (message[0] === 'delete') {
          delete data[message[1]];
        }
      });
      inited = true;
    }

    return $http.get(`${baseUrl}`)
      .then((res) => {
        res.data.forEach(item => {
          if (data.hasOwnProperty(item._id)) {
            Object.assign(data[item._id], item);
          } else {
            data[item._id] = item;
          }
        });
        return data;
      });
  };

  const get = (id) => {
    return data[id];
  };

  const create = (data) => {
    return $http.post(`${baseUrl}`, data);
  };

  const read = (id) => {
    return $http.get(`${baseUrl}/${id}`)
      .then((res) => {
        if (data.hasOwnProperty(id)) {
          Object.assign(data[id], res.data);
          return data[id];
        } else {
          data[id] = res.data;
        }
      })
      .catch(() => {
        delete data[id];
      });
  };

  const me = () => {
    return $http.get(`${baseUrl}/me`)
      .then((res) => {
        return res.data;
      });
  };

  const update = (id, data) => {
    return $http.put(`${baseUrl}/${id}`, data);
  };

  const remove = (id) => {
    return $http.delete(`${baseUrl}/${id}`);
  };

  return {
    list,
    get,
    create,
    read,
    me,
    update,
    remove
  };
}]);
