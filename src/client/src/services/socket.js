angular.module('app').factory('socket', function ($rootScope) {
  let socket;
  let refresh = true;
  return {
    socket,
    connect: (token) => {
      if(socket) {
        return Promise.resolve(socket);
      } else {
        return new Promise((resolve, reject) => {
          refresh = true;
          socket = io({
            query: {
              token
            }
          });
          socket.on('connect', () => {
            resolve(socket)
          });
          socket.on('connect_error', (error) => {
            debugger;
          });
          socket.on('connect_timeout', (timeout) => {
            debugger;
          });
          socket.on('error', (error) => {
            debugger;
          });
          socket.on('disconnect', (reason) => {
            if (refresh) {
              window.location.reload();
            }
          });
        });
      }
    },
    close: () => {
      refresh = false;
      socket.close();
      socket = null;
    },
    on: function (eventName, callback) {
      socket.on(eventName, function () {
        const args = arguments;
        $rootScope.$apply(function () {
          callback.apply(socket, args);
        });
      });
    },
    emit: function (eventName, data, callback) {
      socket.emit(eventName, data, function () {
        const args = arguments;
        $rootScope.$apply(function () {
          if (callback) {
            callback.apply(socket, args);
          }
        });
      })
    }
  };
});