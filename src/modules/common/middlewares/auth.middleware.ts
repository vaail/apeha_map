import {
  Injectable,
  NestMiddleware,
  HttpException,
  HttpStatus,
} from '@nestjs/common';
import { JwtService } from '../../auth/jwt/jwt.service';

@Injectable()
export class AuthMiddleware implements NestMiddleware {
  constructor(private readonly jwtService: JwtService) {}

  async resolve() {
    return async (req, res, next) => {
      const authorization = req.headers.authorization as string;

      if (authorization && authorization.split(' ')[0] === 'Bearer') {
        const token = authorization.split(' ')[1];

        try {
          req.user = await this.jwtService.verify(token);
        } catch (e) {
          throw new HttpException('Unauthorized access', HttpStatus.BAD_REQUEST);
        }

        next();
      } else {
        throw new HttpException('Unauthorized access', HttpStatus.BAD_REQUEST);
      }
    };
  }
}
