angular.module('app').component('mapFormComponent', {
  templateUrl: '/src/components/map-form/map-form.component.html',
  controller: function (mapsService) {
    this.form = {};
    this.save = () => {
      this.item._id
        ? mapsService.update(this.item._id, {
          _id: this.item._id,
          ...this.form
        })
        : mapsService.create({
          ...this.form
        });
    };

    this.remove = () => {
      this.item._id
        ? mapsService.remove(this.item._id)
        : (() => {})();
    };

    this.$onChanges = (data) => {
      if (data.item.currentValue) {
        this.form = data.item.currentValue;
      }
    };
  },
  bindings: {
    item: '<',
    users: '<',
  }
});