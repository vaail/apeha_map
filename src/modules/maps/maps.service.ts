import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';

import { Map } from './interfaces/map.interface';
import { ObjectId } from 'bson';

@Injectable()
export class MapsService {
  constructor(@InjectModel('Map') private readonly MapModel: Model<Map>) {}

  async create(map: Map): Promise<Map> {
    const createdMap = new this.MapModel(map);
    return await createdMap.save();
  }

  async findAll(options?: any): Promise<Map[]> {
    const maps = await this.MapModel.find(options).exec();
    const serializedMaps = maps.map(map => {
      return map.schema.methods.serialize(map);
    });

    return serializedMaps;
  }

  async findById(id: string): Promise<Map | null> {
    let map = await this.MapModel.findById(id).exec();

    if (map) {
      map = map.schema.methods.serialize(map);
    }

    return map;
  }

  async findOne(
    options: any,
    fields?: any,
    isSerialized?: boolean,
  ): Promise<Map | null> {
    let map = await this.MapModel.findOne(options, fields).exec();
    if (map && isSerialized) {
      map = map.schema.methods.serialize(map);
    }

    return map;
  }

  async update(id: number, newValue: Map): Promise<Map | null> {
    return await this.MapModel.findByIdAndUpdate(id, newValue).exec();
  }

  async action(id: number, body): Promise<Map | null> {
    const map = await this.MapModel.findOne({ _id: new ObjectId(id) }).exec();
    const { action, field, user, username } = body;
    const data = {};
    if (action === 'markMe') {
      data[`map.${field}`] = {
        type: 4,
        data: {
          username: user.username,
        },
      };
//
      const oldIndex = map.map.findIndex(cell => cell.data && cell.data.username === user.username);
      if (oldIndex !== -1) {
        data[`map.${oldIndex}`] = {
          type: 0,
          data: {},
        };
      }
    } else if (action === 'markEnemy') {
      data[`map.${field}`] = {
        type: 2,
        data: {
          username,
          crazy: null,
          fear: null,
          freeze: null,
          immune: null,
        },
      };
    } else if (action === 'markReset') {
      data[`map.${field}.data.crazy`] = null;
      data[`map.${field}.data.fear`] = null;
      data[`map.${field}.data.freeze`] = null;
      data[`map.${field}.data.immune`] = null;
    } else if (action === 'markCrazy' && map.map[field].type === 2) {
      data[`map.${field}.data.crazy`] = true;
    }else if (action === 'markFear' && map.map[field].type === 2) {
      data[`map.${field}.data.fear`] = true;
    }else if (action === 'markFreeze' && map.map[field].type === 2) {
      data[`map.${field}.data.freeze`] = true;
    }else if (action === 'markImmune' && map.map[field].type === 2) {
      data[`map.${field}.data.immune`] = true;
    } else if (action === 'emptyCell') {
      data[`map.${field}`] = {
        type: 0,
        data: {},
      };
    } else if (action === 'emptyMap' && user.role === 'admin') {
      for (let i = 0; i < 180; i++) {
        data[`map.${i}`] = {
          type: 0,
          data: {},
        };
      }
    }

    return await this.MapModel.update({
      _id: new ObjectId(id),
    }, {
        $set: data,
      },
    ).exec();
  }

  async delete(id: number): Promise<Map | null> {
    return await this.MapModel.findByIdAndRemove(id).exec();
  }
}
