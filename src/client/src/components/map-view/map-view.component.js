angular.module('app').component('mapViewComponent', {
  templateUrl: '/src/components/map-view/map-view.component.html',
  controller: function(mapsService) {
    this.rowsCount = 12;
    this.cellsCount = 15;

    this.rows = Array.apply(null, Array(this.rowsCount)).map((v, i) => i);
    this.cols = Array.apply(null, Array(this.cellsCount)).map((v, i) => i);

    this.$onChanges = (data) => {
      // if (data.item.currentValue) {
      //   this.data =  data.item.currentValue.map;
      // }
    };
    // this.data = Array.apply(null, Array(this.rowsCount * this.cellsCount)).map(() => ({
    //   type: 0,
    //   data: {}
    // }));

    this.selected = null;

    const coordsMap = {};
    for (let box = 0; box < 6; box++) {
      coordsMap[box + 1] = {};
      let rowM = box < 3 ? 0 : 1;
      let colM = box % 3;
      for (let j = 6 * rowM; j < 6 + 6 * rowM; j++) {
        for (let i = 1 + 5 * colM; i < 6 + 5 * colM; i++) {
          coordsMap[box + 1][i + j * 15] = i - (5 * colM) + (j - (6 * rowM)) * 5;
        }
      }
    }

    this.getPosition = (position) => {
      let result = null;
      Object.keys(coordsMap).forEach(box => {
        if (coordsMap[box].hasOwnProperty(position + 1)) {
          result = `${box}:${coordsMap[box][position + 1]}`;
        }
      });
      return result;
    };


    this.select = (index) => {
      this.selected = index;
    };
    this.markMe = () => {
      if (this.selected === null) return;
      if (this.item.map[this.selected].type !== 0) {
        if(!confirm('Клетка занята. Продолжить?')) {
          return;
        }
      }

      // let oldPosition = null;
      // this.item.map.forEach((val, index) => {
      //   if (this.selected === index) {
      //     val.type = 4;
      //   } else if (this.selected !== index && val.type === 4) {
      //     oldPosition = index;
      //     val.type = 0;
      //   }
      // });
      mapsService.action(this.item._id, {
        action: 'markMe',
        field: this.selected,
      });
      this.selected = null;
    };
    this.markEnemy = () => {
      if (this.selected === null) return;

      // this.item.map[this.selected].type = 2;
      const username = prompt('Ник врага');
      mapsService.action(this.item._id, {
        action: 'markEnemy',
        field: this.selected,
        username,
      });
      this.selected = null;
    };
    this.markReset = () => {
      if (this.selected === null) return;
      if ([2].indexOf(this.item.map[this.selected].type) === -1) {
        return;
      }

      // this.item.map[this.selected].type = 2;
      mapsService.action(this.item._id, {
        action: 'markReset',
        field: this.selected,
      });
      this.selected = null;
    };
    this.markCrazy = () => {
      if (this.selected === null) return;
      if ([2].indexOf(this.item.map[this.selected].type) === -1) {
        return;
      }

      // this.item.map[this.selected].type = 2;
      mapsService.action(this.item._id, {
        action: 'markCrazy',
        field: this.selected,
      });
    };
    this.markFear = () => {
      if (this.selected === null) return;
      if ([2].indexOf(this.item.map[this.selected].type) === -1) {
        return;
      }

      // this.item.map[this.selected].type = 2;
      mapsService.action(this.item._id, {
        action: 'markFear',
        field: this.selected,
      });
    };
    this.markFreeze = () => {
      if (this.selected === null) return;
      if ([2].indexOf(this.item.map[this.selected].type) === -1) {
        return;
      }

      // this.item.map[this.selected].type = 2;
      mapsService.action(this.item._id, {
        action: 'markFreeze',
        field: this.selected,
      });
    };
    this.markImmune = () => {
      if (this.selected === null) return;
      if ([2].indexOf(this.item.map[this.selected].type) === -1) {
        return;
      }

      // this.item.map[this.selected].type = 2;
      mapsService.action(this.item._id, {
        action: 'markImmune',
        field: this.selected,
      });
    };
    this.emptyCell = () => {
      if (this.selected === null) return;
      if (this.item.map[this.selected].type === 0) return;
      if(!confirm('Юнит будет удалён. Продолжить?')) {
        return;
      }

      // this.item.map[this.selected].type = 0;
      mapsService.action(this.item._id, {
        action: 'emptyCell',
        field: this.selected,
      });
      this.selected = null;
    };
    this.emptyMap = () => {
      if(!confirm('Карта будет удалена. Продолжить?')) {
        return;
      }
      // this.item.map.forEach((val) => {
      //   val.type = 0;
      // });
      mapsService.action(this.item._id, {
        action: 'emptyMap',
      });
    };
  },
  bindings: {
    item: '<',
  },
});