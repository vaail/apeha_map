import { Document } from 'mongoose';

export interface Map extends Document {
  readonly name: string;
  readonly color: string;
  readonly map: any;
  readonly userAccess: any;
  readonly created_at: Date;
  readonly updated_at: Date;
}
