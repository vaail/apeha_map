import {
  Controller,
  Get,
  Post,
  Delete,
  Body,
  Request,
  HttpException,
  HttpStatus,
  Put, Req,
} from '@nestjs/common';

import { UsersService } from './users.service';
import { User } from './interfaces/user.interface';
import { Roles } from '../common/decorators/roles.decorator';
import { EventsGateway } from '../events/events.gateway';

@Controller('users')
@Roles('admin')
export class UsersController {
  constructor(
    private readonly usersService: UsersService,
    private readonly eventsGateway: EventsGateway,
  ) {}

  @Get()
  async index(@Req() req): Promise<User[]> {
    const userId = req.user._id.toString();
    if (this.eventsGateway.connectedUsers.hasOwnProperty(userId)) {
      Object.keys(this.eventsGateway.connectedUsers[userId]).forEach(id => {
        const client = this.eventsGateway.connectedUsers[userId][id];
        if (client.rooms.indexOf('users') === -1) {
          client.client.join('users');
          client.rooms.push('users');
        }
      });
    }

    return await this.usersService.findAll();
  }

  @Roles('admin', 'member')
  @Get('/me')
  async me(@Req() req): Promise<User> {
    return this.usersService.findById(req.user._id);
  }

  @Get(':id')
  async show(@Request() req): Promise<User> {
    const id = req.params.id;
    if (!id)
      throw new HttpException(
        'ID parameter is missing',
        HttpStatus.BAD_REQUEST,
      );

    const user = await this.usersService.findById(id);
    if (!user)
      throw new HttpException(
        `The user with the id: ${id} does not exists`,
        HttpStatus.BAD_REQUEST,
      );

    return user;
  }

  @Post()
  async create(@Body() body, @Req() req) {
    if (!body || (body && Object.keys(body).length === 0))
      throw new HttpException('Missing informations', HttpStatus.BAD_REQUEST);

    const item = await this.usersService.create(body);

    const userId = req.user._id.toString();
    if (this.eventsGateway.connectedUsers.hasOwnProperty(userId)) {
      const ids = Object.keys(this.eventsGateway.connectedUsers[userId]);
      if (ids.length) {
        this.eventsGateway.connectedUsers[userId][ids[0]].client.broadcast
          .to('users')
          .emit(
            'users',
            ['create', item._id.toString()],
          );
      }
    }
  }

  @Put(':id')
  async update(@Request() req) {
    const id = req.params.id;
    if (!id)
      throw new HttpException(
        'ID parameter is missing',
        HttpStatus.BAD_REQUEST,
      );

    await this.usersService.update(id, req.body);

    const userId = req.user._id.toString();
    if (this.eventsGateway.connectedUsers.hasOwnProperty(userId)) {
      const ids = Object.keys(this.eventsGateway.connectedUsers[userId]);
      if (ids.length) {
        this.eventsGateway.connectedUsers[userId][ids[0]].client.broadcast
          .to('users')
          .emit(
            'users',
            ['update', id],
          );
      }
    }
  }

  @Delete(':id')
  public async delete(@Request() req) {
    const id = req.params.id;
    if (!id)
      throw new HttpException(
        'ID parameter is missing',
        HttpStatus.BAD_REQUEST,
      );

    await this.usersService.delete(id);

    const userId = req.user._id.toString();
    if (this.eventsGateway.connectedUsers.hasOwnProperty(userId)) {
      const ids = Object.keys(this.eventsGateway.connectedUsers[userId]);
      if (ids.length) {
        this.eventsGateway.connectedUsers[userId][ids[0]].client.broadcast
          .to('delete')
          .emit(
            'users',
            ['update', id],
          );
      }
    }
  }
}
