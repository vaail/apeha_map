export const APP_CONFIG = {
  accessTokenExpires: '24h',
  refreshTokenExpires: '168h',
  jwtSecret: 'MyS3ecr3tK3Y222e',
  jwtSession: {
    session: false,
  },
  databaseURL: 'mongodb://root:map@mongo:27017/admin',
};