import { Schema } from 'mongoose';

const user = new Schema({
  username: { type: String, required: true, unique: true },
  password: { type: String, required: true },
  role: { type: String, required: true },
  created_at: { type: Date, default: Date.now },
  updated_at: { type: Date, default: Date.now },
});

user.index({ username: 'text' });

/**
 * On every save, add the date
 */
user.pre('save', function(next) {
  const currentDate = new Date();

  // @ts-ignore
  this.updated_at = currentDate;
  next();
});

/**
 * Serialize user to send it throw the JWT token
 */
user.methods.serialize = (_user) => {
  return {
    _id: _user._id,
    username: _user.username,
    role: _user.role,
  };
};

export const UserSchema = user;
