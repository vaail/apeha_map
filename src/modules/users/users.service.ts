import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';

import { User } from './interfaces/user.interface';

@Injectable()
export class UsersService {
  constructor(@InjectModel('User') private readonly UserModel: Model<User>) {}

  async create(user: User): Promise<User> {
    const createdUser = new this.UserModel(user);
    return await createdUser.save();
  }

  async findAll(options?: any): Promise<User[]> {
    const users = await this.UserModel.find(options).exec();
    const serializedUsers = users.map(user => {
      return user.schema.methods.serialize(user);
    });

    return serializedUsers;
  }

  async findById(id: string): Promise<User | null> {
    let user = await this.UserModel.findById(id).exec();

    if (user) {
      user = user.schema.methods.serialize(user);
    }

    return user;
  }

  async findOne(
    options: any,
    fields?: any,
    isSerialized?: boolean,
  ): Promise<User | null> {
    let user = await this.UserModel.findOne(options, fields).exec();
    if (user && isSerialized) {
      user = user.schema.methods.serialize(user);
    }

    return user;
  }

  async update(id: number, newValue: User): Promise<User | null> {
    const data = { ...newValue };
    const item = await this.UserModel.findOne({ _id: new Object(id) });
//
    if (!data.password) {
      // @ts-ignore
      data.password = item.password;
    }

    return await this.UserModel.findByIdAndUpdate(id, data).exec();
  }

  async delete(id: number): Promise<User | null> {
    return await this.UserModel.findByIdAndRemove(id).exec();
  }
}
